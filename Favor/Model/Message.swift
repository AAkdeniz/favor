//
//  Message.swift
//  Favor
//
//  Created by Ahmet Akdeniz on 2019-08-24.
//  Copyright © 2019 Favor. All rights reserved.
//

import UIKit
import Firebase

class Message: NSObject {
    var fromId: String?
    var text: String?
    var timestamp: NSNumber?
    var toId: String?
    
    func chatPartnerId() -> String?{
        return fromId == Auth.auth().currentUser?.uid ? toId : fromId
    }
}
