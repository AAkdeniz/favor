//
//  User.swift
//  Favor
//
//  Created by Ahmet Akdeniz on 2019-08-08.
//  Copyright © 2019 Favor. All rights reserved.
//

import UIKit

class User: NSObject{
    var id: String?
    var username: String?
    var email: String?
    var profileImageUrl: String?
}
