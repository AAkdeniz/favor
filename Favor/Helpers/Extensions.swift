//
//  Extensions.swift
//  Favor
//
//  Created by Ahmet Akdeniz on 2019-08-09.
//  Copyright © 2019 Favor. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {

    func loadImageUsingCacheWithUrlString(urlString:String){
        //Check cache for image first
        self.image = nil
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = cachedImage
            return
        }
        //Otherwise new download
    let url = URL(string: urlString)
    URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
    
    if error != nil{
    print(error)
    return
    }
    
    DispatchQueue.main.async {
        if let downloadedImage = UIImage(data: data!){
            imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
            self.image = downloadedImage
        }
    }
        }).resume()
    }
}


