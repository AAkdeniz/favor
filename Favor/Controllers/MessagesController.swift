//
//  MainViewController.swift
//  Favor
//
//  Created by Ahmet Akdeniz on 2019-08-20.
//  Copyright © 2019 Favor. All rights reserved.
//

import UIKit
import Firebase

class MessagesController: UITableViewController {

    let cellId = "cellId"
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        
        let newMessage = UIImage(named: "new_message")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: newMessage, style: .plain, target: self, action: #selector(handleNewMessage))
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        checkUserLoggedIn()
    }
    
    var messages = [Message]()
    var messagesDictionary = [String: Message]()
    
    func observeUserMessages(){
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
         let ref = Database.database().reference().child("user-messages").child(uid)
        ref.observe(.childAdded, with: {(snapshot) in
            let messageId = snapshot.key
            self.fetchMessageWithMessageId(messageId: messageId)
        }, withCancel: nil)
    }
    
    private func fetchMessageWithMessageId(messageId: String){
        let messsagesReference = Database.database().reference().child("messages").child(messageId)
        messsagesReference.observeSingleEvent(of: .value, with: {(snapshot) in
            
            if let dictionary = snapshot.value as? [String: Any]{
                let message = Message()
                message.text =  dictionary["text"] as? String
                message.fromId =  dictionary["fromId"] as? String
                message.toId =  dictionary["toId"] as? String
                message.timestamp =  dictionary["timestamp"] as? NSNumber
                
                // Append the created message to the messages array
                self.messages.append(message)
                
                // Use Hash (Dictionary) to group the messages under the per receiver
                if let chatPartnerId = message.chatPartnerId(){
                    self.messagesDictionary[chatPartnerId] = message
                }
                
                self.attemptReloadOfTable()
                }
                }, withCancel: nil)
    }

    
    private func attemptReloadOfTable(){
    // To avoid reloading the table view every time, added a timer and when each cell called timer becomes invalid except last time
    self.timer?.invalidate()
    self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    var timer: Timer?
    
    @objc func handleReloadTable(){
        self.messages = Array(self.messagesDictionary.values)
        
        // For sorting messages based on timestamp
        self.messages.sort(by: {(message1, message2)-> Bool in
            return message1.timestamp!.intValue > message2.timestamp!.intValue
        })
        self.tableView.reloadData()
    }
    
    func observeMessages(){
        let ref = Database.database().reference().child("messages")
        ref.observe(.childAdded, with: {(snapshot) in
           
            if let dictionary = snapshot.value as? [String: Any]{
                let message = Message()
                message.text =  dictionary["text"] as? String
                message.fromId =  dictionary["fromId"] as? String
                message.toId =  dictionary["toId"] as? String
                message.timestamp =  dictionary["timestamp"] as? NSNumber
                
                // Append the created message to the messages array
                self.messages.append(message)
                
                // Use Hash (Dictionary) to group the messages under the per receiver
                if let toId = message.toId{
                    self.messagesDictionary[toId] = message
                    
                    self.messages = Array(self.messagesDictionary.values)
                    
                    // For sorting messages based on timestamp
                    self.messages.sort(by: {(message1, message2)-> Bool in
                        return message1.timestamp!.intValue > message2.timestamp!.intValue
                    })
                }
                self.tableView.reloadData()
            }
            
           
        }, withCancel: nil)
    }
    
    func checkUserLoggedIn(){
        //User is not logged in
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        }else{
            fetchUserAndSetNavBarTitle()
        }
    }
    
    func fetchUserAndSetNavBarTitle(){
        guard let uid = Firebase.Auth.auth().currentUser?.uid else { return }
        Database.database().reference().child("users").child(uid).observeSingleEvent(of:.value, with: {(snapshot) in
            
            //Fetch the name from db and assign it to the navigationItem title
            if let dictionary = snapshot.value as? [String: AnyObject]{
                //self.navigationItem.title = dictionary["username"] as? String
              
                let user = User()
                //user.setValuesForKeys(dictionary)
                
                user.username = dictionary["username"] as? String
                user.email =  dictionary["email"] as? String
                user.profileImageUrl =  dictionary["profileImageUrl"] as? String
                
                self.setupNavBarWithUser(user: user)
            }
            
        }, withCancel:nil)
    }
    
    
    func setupNavBarWithUser(user: User){
        messages.removeAll()
        messagesDictionary.removeAll()
        tableView.reloadData()
        
        // To fill the messages of the current user call the observe func
        observeUserMessages()
        
        let titleView = UIView()
        titleView.frame = CGRect(x:0, y:0, width: 100, height: 40)
     
        // Created for making name label as long as it needs
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        titleView.addSubview(containerView)
        
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.cornerRadius = 20
        profileImageView.clipsToBounds = true
        if let profileImageUrl = user.profileImageUrl{
            profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)

        
        containerView.addSubview(profileImageView)
        
        // Need x, y, width, height
        profileImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let nameLabel = UILabel()
        containerView.addSubview(nameLabel)
        
        nameLabel.text = user.username
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // Need x, y, width, height
        nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalTo: profileImageView.heightAnchor).isActive = true
  
        // Need x, y, width, height
        containerView.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        
        self.navigationItem.titleView = titleView
        }
    }
    func showChatControllerForUser(user: User){
        // Changed UIView to Collection view for merging table view and collection view in same page
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatLogController.user = user
        navigationController?.pushViewController(chatLogController, animated: true)
    }
    
    @objc func handleLogout(){
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LoginController()
        loginController.messagesController = self
        present(loginController, animated:  true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let message = messages[indexPath.row]
        cell.message = message
      
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = messages[indexPath.row]
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        
        let ref = Database.database().reference().child("users").child(chatPartnerId)
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            let user = User()
            user.id = chatPartnerId
            user.email = dictionary["email"] as? String
            user.profileImageUrl = dictionary["profileImageUrl"] as? String
            user.username =  dictionary["username"] as? String
            
            self.showChatControllerForUser(user: user)
        }, withCancel: nil)
   
    }
    
    @objc func handleNewMessage(){
        let newMessageController = NewMessageController()
        // Having set this variable reach the chat 
        newMessageController.messagesController = self
        let navigationController = UINavigationController(rootViewController: newMessageController)
        present(navigationController, animated: true, completion: nil)
        
    }
}

