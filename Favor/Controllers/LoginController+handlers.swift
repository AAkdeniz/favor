//
//  ProfileController+handlers.swift
//  Favor
//
//  Created by Ahmet Akdeniz on 2019-08-08.
//  Copyright © 2019 Favor. All rights reserved.
//

import UIKit
import Firebase

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
    func handleRegister(){
        guard let email = emailTextField.text, let password = passwordTextField.text, let username = usernameTextField.text else {
            print("Form is not valid")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password, completion: { (authResult, error) -> Void in
            if error != nil{
                print(error!)
                return
            }
            
            guard let uid = authResult?.user.uid else{
                return
            }
            
            // Successfully authenticated user
            let imageName = NSUUID().uuidString
            let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
            
            // To give the photos a quality limitation, change png to jpg and ...
            if let profileImage = self.profileImageView.image,
                let uploadData =  self.profileImageView.image!.jpegData(compressionQuality: 0.1) {
                // if let uploadData = self.profileImageView.image!.pngData() {
                storageRef.putData(uploadData, metadata: nil, completion:
                    { (metadata, error) in
                        if error != nil {
                            print(error)
                            return
                        }
                        
                        storageRef.downloadURL(completion: { (url, error) in
                            if error != nil {
                                print(error)
                                return
                            }
                            if let profileImageUrl = url?.absoluteString {
                                
                                let values = ["username": username, "email": email, "profileImageUrl": profileImageUrl] as [String : AnyObject]
                                self.registerUserIntoDatabaseWithUID(uid: uid, values: values)
                            }
                        })
                })
            }
        })
    }
    private func registerUserIntoDatabaseWithUID(uid: String, values: [String: AnyObject]) {
        let ref = Firebase.Database.database().reference()
        
        let usersReference = ref.child("users").child(uid)
        usersReference.updateChildValues(values, withCompletionBlock: {
            (error, ref) in
            
            //Else print error
            if error != nil {
                print(error!)
                return
            }
           // self.messagesController?.navigationItem.title = values["username"] as? String
            
            let user = User()
            //user.setValuesForKeys(dictionary)
            
            user.username = values["username"] as? String
            user.email =  values["email"] as? String
            user.profileImageUrl =  values["profileImageUrl"] as? String
            
            self.messagesController?.setupNavBarWithUser(user: user)
            
            //Dismissed page after registration done
            self.dismiss(animated: true, completion: nil)
        })
    }

    @objc func handleSelectProfileImageView(){
    
        let imagePicker = UIImagePickerController()
        present(imagePicker, animated: true, completion: nil)
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        let alertController = UIAlertController(title: "Choose Image Source",
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let cameraAction = UIAlertAction(title: "Camera",
                                             style: .default,
                                             handler: { action in
                                                imagePicker.sourceType = .camera
                                                self.present(imagePicker,
                                                             animated: true,
                                                             completion: nil)
                                                
            })
            alertController.addAction(cameraAction)
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let photoLibraryAction = UIAlertAction(title: "Photo Library",
                                                   style: .default,
                                                   handler: { action in
                                                    imagePicker.sourceType = .photoLibrary
                                                    self.present(imagePicker,
                                                                 animated: true,
                                                                 completion: nil)
            })
            alertController.addAction(photoLibraryAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true,completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage{
                selectedImageFromPicker = originalImage
        }
        if let selectedImage = selectedImageFromPicker{
            profileImageView.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
       // assignSelectedImage()
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
