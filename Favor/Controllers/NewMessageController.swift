//
//  NewMessageController.swift
//  Favor
//
//  Created by Ahmet Akdeniz on 2019-08-08.
//  Copyright © 2019 Favor. All rights reserved.
//

import UIKit
import Firebase

class NewMessageController: UITableViewController {
    let cellId = "cellId"
    var users = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        
       tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        fetchUser()
    }
    
    func fetchUser(){
            Database.database().reference().child("users").observe(.childAdded, with: {(snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject]{
                    let user = User()
                    
                    // Assign the Firebase uid to the user id
                    user.id = snapshot.key
                    //Todo: Normally it should work but ..
                  //user.setValuesForKeys(dictionary)
                    
                    //When the code above work delete these two lines
                    user.username = dictionary["username"] as? String
                    user.email =  dictionary["email"] as? String
                    user.profileImageUrl =  dictionary["profileImageUrl"] as? String
                    
                    self.users.append(user)
                    self.tableView.reloadData()
                }
            }, withCancel: nil)
    }

    @objc func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let user = users[indexPath.row]
        cell.textLabel?.text = user.username
        cell.detailTextLabel?.text = user.email
        
        if let profileImageUrl = user.profileImageUrl{
            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
    }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    var messagesController: MessagesController?
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // dismiss the page
        self.dismiss(animated: true, completion: nil)
        // Select the user in the table view
        let user = self.users[indexPath.row]
        self.messagesController?.showChatControllerForUser(user: user)
    }
}
